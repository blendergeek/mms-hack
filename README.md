# MMS Hack

This is a hack to let you read MMS on your Pinephone. It is not intended for production. Please
do not use it for any purpose. If you find it useful, thats great. It is not secure. I use it
for my own personal use. There is no warranty. If it fails you or unleashes nasal demons, do
not complain.

In order to use this hack, you will need to compile mms-hack-parser.c and place 'mms-hack-parser'
in your path. I run `gcc -o mms-hack parser mms-hack-parser.c` to compile it. Then run
`mms-hack list` to get a list of SMS on your modem. Run `mms-hack show SMS` where SMS is
the id of the SMS you want to view to see the contents of an SMS on the modem. Run
`mms-hack retrieve SMS` where SMS is the ID of the SMS whose MMS contents you wish to
view to retrieve the contents of an MMS and view them.
