#include <stdio.h>
#include <stdlib.h>

/*
This program takes two arguments.
The first is either an 'f' or an 'm'.
The second is a filename.
The program opens the file specified by the second argument
and parses it as an SMS message as output by mmcli.
If the first argument is an 'f', the program outputs the 'from'
portion of the SMS. If the first argument is an 'm', it outputs
the body of the message.
*/
int main(int argc, char **argv) {
  if (argc == 1) {
    fprintf(stderr, "No arguments supplied.\n");
    return EXIT_FAILURE;
  }
  char message[250];
  char buffer[4] = {0, 0, 0, 0};
  int i = 0;
  char messagebuffer[2000];
  char frombuffer[2000];
  int messagebegan = 0;
  int frombegan = 0;
  FILE * mms;
  mms = fopen(argv[2], "r");
  while(fread(buffer, 1, 1, mms)) {
    buffer[3] = buffer[2];
    buffer[2] = buffer[1];
    buffer[1] = buffer[0];
    if ((buffer[1] == 0203) && (buffer[2] == 0x18) && (buffer[3] == 0200)) {
      frombegan = 1;
      messagebegan = 0;
      i = 0;
      continue;
    }
    if ((buffer[1] == 0203) && (buffer[2] == 0200) && (buffer[3] == 0364)) {
      messagebegan = 1;
      i = 0;
      frombegan = 0;
      continue;
    }
    if ((buffer[1] == 0203) && (buffer[2] == 0134) && (buffer[3] == 0364)) {
      messagebegan = 1;
      i = 0;
      frombegan = 0;
      continue;
    }
    if ((buffer[1] == 0203) && (buffer[2] == 0134) && (buffer[3] == 026)) {
      messagebegan = 1;
      i = 0;
      frombegan = 0;
      continue;
    }
    if (frombegan) {
      if (*buffer)
	frombuffer [i++] = *buffer;
      else
	frombegan = 0;
    }
    
    if (messagebegan) {
      if (*buffer)
	messagebuffer [i++] = *buffer;
      else
	messagebegan = 0;
    }
  }
  if (argv[1][0] == 'f')
    printf ("%s", frombuffer);
  if (argv[1][0] == 'm')
    printf ("%s", messagebuffer);
}
