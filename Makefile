all: mms-hack-parser mms-hack-parse-mms

mms-hack-parser:
	gcc -o mms-hack-parser mms-hack-parser.c

mms-hack-parser-mms:
	gcc -o mms-hack-parse-mms mms-hack-parse-mms.c
install:
	cp -f mms-hack mms-download mms-hack-parse-mms mms-hack-parser /usr/local/bin
clean:
	rm mms-hack-parser mms-hack-parse-mms
