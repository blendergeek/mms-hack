#include <stdio.h>
#include <stdlib.h>

/* This program takes an MMS message as an argument and outputs the body to the command line. */

#define MAX 10000

int main() {
  int i=0;
  char messagebuffer[10000];
  char cbuffer[1];
  while (fgets(cbuffer, 1, stdin)) {
    if (cbuffer[0]) {
      messagebuffer[i] = cbuffer[0];
      i++;
      if (i>=(MAX-1))
	break;
    }
    else {
      i=0;
    }
  }
  messagebuffer[i] = 0;
  printf("%s", messagebuffer);
}
